@extends('layouts.app')

@section('content')

<div class="jumbotron bg-dark text-white ">
  <br><br><br><br>
    <h1 class="display-2 d-flex justify-content-center ">Bienvenue </h1>
    <p class="display-4 d-flex justify-content-center ">sur Netflix lowcost</p>
    <hr class="my-1 d-flex justify-content-center bg-danger  ">
    <p class="d-flex justify-content-center">Ici c'est comme Netflix ... mais tu pourras seulement voir les affiches ! </p>
    <p class="lead d-flex justify-content-center ">
      <a class="btn btn-danger btn-lg" href="/login" role="button" class='d-flex justify-content-center'>Connexion</a>
    </p>
    <a href="/register"><p class='d-flex justify-content-center'>Pas encore inscrit ? clic ici </p></a>
    <br><br><br><br>
  </div>

@endsection