@extends('layouts.app')

@section('content')
<br><br><br><br>
{{-- _______TITLE________ --}}
<div>
    <h1>- Update movie -</h1>
</div>
<br><br><br><br>

<div class="container ">
    <form action="/movie/addupdate/bdd" method="POST">
        @csrf

{{-- _________INPUT UPDATE__________ --}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <label for="title" class="card-header text-dark">{{ $data['Title'] }}
                            <input hidden id="title" name="title" type="text" value="{{ $data['Title'] }}" />
                        </label>
                            <div class="card-body">
                                {{-- Poster--}}
                                <div class="form-group row">
                                        <label for="poster" class="col-md-4 col-form-label text-dark text-md-right">Poster :
                                            <input hidden id="poster" name="poster" type="text" value="{{ $data['Poster'] }}"/>
                                        </label>
                                        <div class="col-md-6">
                                            <img src=" {{ $data['Poster'] }} " alt="film poster">
                                        </div>
                                    </div>
                                    {{-- ___________ --}}
                                {{-- RESUME--}}
                                <div class="form-group row">
                                    <label for="resume" class="col-md-4 col-form-label text-dark text-md-right">Resume :</label>
                                    <div class="col-md-6">
                                        <textarea id="resume" name="resume" cols="30" rows="7">{{ $data['Plot'] }}</textarea>
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{-- YEARS--}}
                                <div class="form-group row">
                                    <label for="year" class="col-md-4 col-form-label  text-dark text-md-right">Years :</label>
                                    <div class="col-md-6">
                                        <input id="year" type="text" name="year" value="{{ $data['Year'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--RELEASED --}}
                                <div class="form-group row">
                                    <label for="released" class="col-md-4 col-form-label  text-dark text-md-right">Released :</label>
                                    <div class="col-md-6">
                                        <input id="released" type="text" name="released" value="{{ $data['Released'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Runtime --}}
                                <div class="form-group row">
                                    <label for="runtime" class="col-md-4 col-form-label  text-dark text-md-right">Runtime :</label>
                                    <div class="col-md-6">
                                        <input id="runtime" type="text" name="runtime" value="{{ $data['Runtime'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Genre--}}
                                <div class="form-group row">
                                    <label for="genre" class="col-md-4 col-form-label  text-dark text-md-right">Genre :</label>
                                    <div class="col-md-6">
                                        <input id="genre" type="text" name="genre" value="{{ $data['Genre'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Director--}}
                                <div class="form-group row">
                                    <label for="director" class="col-md-4 col-form-label  text-dark text-md-right">Director :</label>
                                    <div class="col-md-6">
                                        <input id="director" type="text" name="director" value="{{ $data['Director'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Language--}}
                                <div class="form-group row">
                                    <label for="language" class="col-md-4 col-form-label  text-dark text-md-right">Language :</label>
                                    <div class="col-md-6">
                                        <input id="language" type="text" name="language" value="{{ $data['Language'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Country--}}
                                <div class="form-group row">
                                    <label for="country" class="col-md-4 col-form-label  text-dark text-md-right">Country :</label>
                                    <div class="col-md-6">
                                        <input id="country" type="text" name="country" value="{{ $data['Country'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Awards--}}
                                <div class="form-group row">
                                    <label for="awards" class="col-md-4 col-form-label  text-dark text-md-right">Awards :</label>
                                    <div class="col-md-6">
                                        <input id="awards" type="text" name="awards" value="{{ $data['Awards'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Type--}}
                                <div class="form-group row">
                                    <label for="type" class="col-md-4 col-form-label  text-dark text-md-right">Type :</label>
                                    <div class="col-md-6">
                                        <input id="type" type="text" name="type" value="{{ $data['Type'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--DVD--}}
                                <div class="form-group row">
                                    <label for="dvd" class="col-md-4 col-form-label  text-dark text-md-right">DVD :</label>
                                    <div class="col-md-6">
                                        <input id="dvd" type="text" name="dvd" value="{{ $data['DVD'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Production--}}
                                <div class="form-group row">
                                    <label for="production" class="col-md-4 col-form-label  text-dark text-md-right">Production :</label>
                                    <div class="col-md-6">
                                        <input id="production" type="text" name="production" value="{{ $data['Production'] }}" >
                                    </div>
                                </div>
                                {{-- ___________ --}}
                                {{--Available--}}
                                <div class="form-group row">
                                    <label for="available" class="col-md-4 col-form-label  text-dark text-md-right">Available :</label>
                                    <div class="col-md-6">
                                        <select class="custom-select" id="available" name="available">
                                            <option selected value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                {{-- ___________ --}}  
                                <button class="btn btn-outline-secondary " type="submit">Validate</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
{{-- RESULT --}}


    

@endsection