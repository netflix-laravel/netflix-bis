@extends('layouts.app')
@section('content')
{{-- _________TITLE PAGE___________ --}}
<div class="jumbotron bg-dark text-white">
    <br>
    <br>
    <h1>- All movies - </h1>
    <br>
    <br>
    {{-- _________CART ___________ --}}
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm">
                    {{-- ______________SPACE FOR MOVIE ________________________ --}}
                    @for ( $id = 0 ; $id < count($movies) ; $id++ )
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="{{ $movies[$id]['poster'] }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title text-danger">{{ $movies[$id]['title'] }}</h5>
                                <p class="card-text text-secondary">{{ $movies[$id]['resume'] }}</p>
                                <div class="d-flex justify-content-center ">
                                    <form action="/session" method="post">
                                        @CSRF
                                        <button type="submit" class="btn btn-success">ajouté au panier</button>
                                        <input type="hidden" name="action" value="add">
                                        <input type="hidden" name="id" value="{id}">
                                    </form>
                                    <form action="/movie/update" method="post">
                                        @CSRF
                                        <button class="btn btn-light" type="submit">modifier</button>
                                        <input type="hidden" name="id" value="{id}">
                                    </form>
                                    <form action="/movie/delete" method="post">
                                        @CSRF
                                        <button class="btn btn-danger" type="submit">supprimer</button>
                                        <input type="hidden" name="id" value="{id}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection