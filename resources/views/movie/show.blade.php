@extends('layouts.app')

@section('content')
{{-- _________TITLE PAGE___________ --}}
<div>
    <h1>- Search your movie -</h1>
</div>
{{-- _________INPUT SEARCH___________ --}}

<div class="input-group mb-3 row justify-content-center">
<form action="/movie/show" method="POST">
    @csrf
    <label for="title"></label>
        <input class="form-control" type="text" name="title">
    <button class="btn btn-outline-secondary" type="submit">Search</button>
</form>
</div>
{{-- _________RESULT SEARCH___________ --}}

<div class="card-columns">
		@if((isset($movies_list)))
		@foreach ($movies_list as $id => $movie)
		<div class="card">
		<img class="card-img-top img-thumbnail" width="200px" height="250px" src="{{$movie['Poster']}}" alt="Card image cap">
		  <div class="card-body">
			<h5 class="card-title">{{ $movie['Title'] }}</h5>
			<p class="card-text text-dark">Type : {{ $movie['Type'] }}</p>
			<p class="card-text text-dark">Year : {{ $movie['Year'] }}</p>
			<form action="/movie/update" method="POST">
				@csrf
				<label for="imdbID"></label>
				<input hidden id='imdbID' name="imdbID" value={{ $movie['imdbID'] }} />
				<button class="btn btn-outline-danger"  type="submit">Add</button>
			</form>
		  </div>
		</div>
		@endforeach
@endif 
		
	  </div>
	  
		  </div>  
{{-- _________MAXIME WORK___________ --}}
  
{{-- @if((isset($movies_list)))
    @foreach ($movies_list as $id => $movie)
    {{-- Layout Exmple--}}
        {{-- <img src={{ $movie['Poster'] }}>
        <p>Title : {{ $movie['Year'] }}</p>
        <p>Type : {{ $movie['Type'] }}</p>
        <form action="/movie/update" method="POST">
            @csrf
            <label for="imdbID"></label>
            <input hidden id='imdbID' name="imdbID" value={{ $movie['imdbID'] }} />
            <button  hidden type="submit">Add</button>
        </form> --}}
    {{-- @endforeach
@endif  --}}

@endsection