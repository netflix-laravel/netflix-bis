@extends('layouts.app')

@section('content')

<div class="jumbotron bg-dark text-white">

    <h1 class="display-2 d-flex justify-content-center "> - Merci - </h1>
    <p class="display-4 d-flex justify-content-center ">pour votre commande</p>
    <hr class="my-1 d-flex justify-content-center bg-danger  ">
    <br>
    <h3 class="text-light"> N'hesiter pas a revoir les films que vous avez louper ! </h3>
    <a href="/movie"><p class=" d-flex justify-content-center"> Show me all movie </p></a>
</div>

@endsection