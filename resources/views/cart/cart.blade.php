@extends('layouts.app')

@section('content')
{{-- _________TITLE PAGE___________ --}}

<div class="jumbotron bg-dark text-white">
    <h1> - Mon  panier - </h1>
</div>
{{-- _________TABLE CART ___________ --}}
<div class=" d-flex justify-content-center " >
        <table class="w-75 p-3 table table-striped table-dark ">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">TITRE</th>
                    <th scope="col">GENRE</th>
                    <th scope="col">_</th>
                  </tr>
                </thead>
                <tbody>
                    {{-- _________SPACE FOR VARIABLE OF MOVIE___________ --}}
                  <tr>
                    <th scope="row">ID : {{1}}</th>
                  <td> titre movie 1</td> {{--}} variable recupérer titre {{--}}
                  <td>genre</td> {{--}} variable recupérer genre film {{--}}
                    <td>
                            <form action="/delete" method="post">

                                <button class="btn btn-danger" type="submit">delete</button>
                                <input type="hidden" name="id" value="{id}">
                                <input type="hidden" name="action" value="del">
                            </form>
                    </td>
                    
                  </tr>
                  
                </tbody>
              </table> 
            </div>
    <br><br>
    {{-- _________BUTTON : VALIDATE & RETURN HOME___________ --}}
            <div class=" d-flex justify-content-center ">
                <form action="" method="">
                    <button class="btn btn-success" type="submit">Validate my Cart</button>
                    <input type="hidden" name="" value="">
                </form>
                <br>
                <form action="/movie" method="">
                        <button class="btn btn-light" type="submit">Return All Movies</button>
                        <input type="hidden" name="" value="">
                </form>
            </div>
        <hr>

@endsection