<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // -----> ADMINISTRATEUR 
        DB::table('users')->insert([
            'name' => 'Administateur',
            'email' => 'adress@adress.fr',
            'password' => bcrypt('password'),
        ]);


        DB::table('roles')->insert([
            'name' => 'Administrateur',
            'slug' => 'admin',
        ]);
        DB::table('role_users')->insert([
            'user_id' => '1',
            'role_id' => '1',
        ]);

        // -----> USER -> utilisateur  
        DB::table('users')->insert([
            'name' => 'Utilisateur',
            'email' => 'adressuser@adress.fr',
            'password' => bcrypt('password'),
        ]);


        DB::table('roles')->insert([
            'name' => 'Utilisateur',
            'slug' => 'user',
        ]);
        DB::table('role_users')->insert([
            'user_id' => '2',
            'role_id' => '2',
        ]);    
        
        
    }
}
