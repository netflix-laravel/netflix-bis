<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title'); //-----> titre
            $table->text('resume');//-------> resumé
            $table->string('year');//-------> année sortie
            $table->string('released');//---> date de sortie
            $table->string('runtime');//----> durée
            $table->string('genre');//------> genre 
            $table->string('director');//---> réalisateur
            $table->string('language');//---> langue
            $table->string('country');//----> pays origine
            $table->string('awards'); //----> recompences
            $table->longText('poster'); //----> lien affiche 
            $table->string('type'); //------> Film ou serie
            $table->string('dvd'); //-------> ??
            $table->string('production');//-> ??
            $table->boolean('available');//-> Disponibilité
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
