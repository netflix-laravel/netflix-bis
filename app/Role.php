<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 
        'slug'
    ];

    // ----> RELATION AVEC TABLE ROLE_USER
    public function role_user()
    {
        return $this->hasMany('App\Role_user');
    }
}
