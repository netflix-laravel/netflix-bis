<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{
    protected $fillable = [
        'user_id', 
        'role_id'
    ];
    //-------> RELATION AVEC TABLE USER
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //-------> RELATION AVEC TABLE ROLE
    public function role()
    {
        return $this->belongsTo('App\Role');
    }
}
