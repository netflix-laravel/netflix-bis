<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bdd_movies = DB::table('movies')->get();
        $movies = json_decode($bdd_movies, true);

        return view('movie/accueil', [
            'movies' => $movies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Si votre programme s'arrête ici, pensez à ajouter la clès API dans .env !
        $api_key = env('API_KEY'); 
        $response_api = file_get_contents('http://www.omdbapi.com/?apikey=' . $api_key . '&i=' . $request->input('imdbID'));
       
        // Transforme l'objet JSON en tableau :
         
        $movie_infos = json_decode($response_api, true);

        return view('movie/update', [
          'data' => $movie_infos
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //test existance d'une requête
        // s'il n'y a pas de requête --> route get :
        if ($request->input() == []) {
            return view('movie/show');
        }
        // sinon / route post
        else {
            
            // Récupère l'input, la clès API & les concatène avec l'url de l'API :
            
            $search = $request->input('title');
            $search = str_replace(' ', '+', $search); // transforme les  ' ' en '+'
            $api_key = env('API_KEY');                // Penser à ajouter la clès API dans .env !
            $response_api = file_get_contents('http://www.omdbapi.com/?apikey=' . $api_key . '&type=movie' . '&s=' . $search);
            
            // Transforme l'objet JSON en tableau :
                
            $response_array = json_decode($response_api, true);
            
            // error management

            if ($response_array['Response'] == 'False') {
                // dd($response_array['Error']);
                $error_message = $response_array['Error'];

                return back()->with('warning', $error_message);
            }
            else {
                // Entre dans la liste de films
                $movies_list = $response_array['Search'];
                
                return view('movie/show', [
                    'movies_list' => $movies_list
                    ]);
            }
        }
    }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       return view('update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $movie = new Movie;
        $movie->title    = $request->input('title');
        $movie->resume   = $request->input('resume');
        $movie->year     = $request->input('year');
        $movie->released = $request->input('released');
        $movie->runtime  = $request->input('runtime');
        $movie->genre    = $request->input('genre');
        $movie->director = $request->input('director');
        $movie->language = $request->input('language');
        $movie->country  = $request->input('country');
        $movie->awards   = $request->input('awards');
        $movie->poster   = $request->input('poster');
        $movie->type     = $request->input('type');
        $movie->dvd      = $request->input('dvd');
        $movie->production = $request->input('production');
        $movie->available  = $request->input('available');

        $movie->save();
        
        return redirect('movie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('movies')->where('id', $id)->delete();    // delete
        redirect ('movie');
    }
}
