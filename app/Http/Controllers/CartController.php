<?php

namespace App\Http\Controllers;
use auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/cart/cart');
    }



    public function session(Request $request)
    {

        
        switch ($request->input('action')) {
            case 'add':
                
                // if (session()->get('panier') ?? '') {
                    //     session()->put('panier', '[]');
                    // }
                    
                    dd("$session('panier')");
                    $add = session('panier');
                    // if ($add == false) {
                        $add = [];
                    // };
                    
                    foreach ($add as $compare) {
                        if ($compare == $request->input('id')) {
                            return back()->with('warning', 'Ce film est dans votre panier.');
                        };
                    }
                    
                    
                            array_push($add,$request->input('id'));
                $request->session()->push('panier', $add);
                return back()->with('success', 'Le film est ajouter au panier.');

                break;
            case 'del':

                $add = session('nbMovie');
                $add -= $request->input('test');
                $request->session()->put('nbMovie', $add);
                return redirect("exemple");

                break;
        }
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //suppr l'id dans session panier 
        return redirect back();
    }
}