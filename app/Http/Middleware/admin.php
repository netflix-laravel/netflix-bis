<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use App\Role_user;
use Illuminate\Support\Facades\Auth;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role= Role::find(Role_user::find(Auth::user()->id)->role_id)->slug;

       if($role != "admin"){
        return back()->with('warning','Vous ne possédez pas les droits administrateurs.');
       }
       return $next($request);
    }
}
