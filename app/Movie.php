<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 
        'resume',
        'year',
        'released',
        'runtime',
        'genre',
        'director',
        'language',
        'country',
        'awards',
        'poster',
        'type',
        'dvd',
        'production',
        'avalaible'
    ];

    // --------------> RELATION TABLE MOVIE_ORDER

    public function Movie_order()
    {
        return $this->hasMany('App\Movie_order');
    }
}
