<?php

namespace App\Providers;

use App\Role;
use App\Role_user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::if('admin', function () {

            $role= Role::find(Role_user::find(Auth::user()->id)->role_id)->slug;
            if($role == "admin"){
            return auth()->check() ;
            };
        });
    }
}
