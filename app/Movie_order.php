<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie_order extends Model
{
    protected $fillable = [
        'movie_id', 
        'order_id'
    ];

    // -----------> RELATION TABLE ORDERS

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    // ----------> RELATION TABE MOVIES

    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }
}
