<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'reference',
        'user_id'
    ];

    // -------------> RELATION TABE USER
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // -------------> RELATION TABE MOVIE ORDER
    public function Movie_order()
    {
        return $this->hasMany('App\Movie_order');
    }
}
