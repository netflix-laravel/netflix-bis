<?php

use App\Http\Middleware\admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('exemple','exempleController@lookExemple');
Route::post('post/exemple','exempleController@addExemple' );

Route::get('/', 'HomeController@index');

////////////////// Movie

Route::get('/movie', 'MovieController@index');

Route::post('/movie/delete', 'MovieController@destroy');

Route::get('/movie/show', 'MovieController@show')->middleware("admin");
Route::post('/movie/show', 'MovieController@show');

Route::post('/movie/update', 'MovieController@create'); //on recupere l'API et on ajoute
Route::get('/movie/update/{id}', 'MovieController@edit'); // on modifie un film deja existant

Route::post('/movie/addupdate/bdd', 'MovieController@update');// on envoie dans la BDD

////////////////// Cart

Route::get('/cart', 'CartController@index');
Route::post('session','CartController@session');

Route::post('/addbdd','CartController@addBDD');


/////////////////// Order
Route::get('/cart/validation', 'OrderController@show');

Route::post('/order', 'OrderController@');